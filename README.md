[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/A70m0s/pymage-stega/-/blob/master/LICENSE)

# Pymage

![Steganography](assets/banner.jpeg "Steganography")

<p style='text-align:center'>→ My first steganography project in Python ←</p>

<div style="text-align:center"><img src="assets/logo.png" /></div>

## How it works ?

It's a very simple encrypting algorythm which replaces the red value of the first pixel with the place of the message's first letter place in the alphabet, then, for the second pixel the place of the message's second letter...

- A simple diagram for a better understanding :

![Steganography](/assets/hiw.png "Steganography")

In this exemple, the pixel has 125 as Red value, and the letter to encrypt is "B", the number 2 in the alphabet. Therefore the pixel after the encrypting has 127 (125 + 2) as Red value. "Simple comme bonjour !" as French people say.

## Meta

[@A70m0s](https://gitlab.com/A70m0s)

Distributed under the MIT license. See ``LICENSE`` for more information.

## Contributing

1. Fork it (<https://gitlab.com/A70m0s/pymage-stega>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request