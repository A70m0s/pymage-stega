'''
Tout ça pour PEP-8 !!!
'''

import sys
from PIL import Image

BANNER = '''

██████╗░██╗░░░██╗███╗░░░███╗░█████╗░░██████╗░███████╗
██╔══██╗╚██╗░██╔╝████╗░████║██╔══██╗██╔════╝░██╔════╝
██████╔╝░╚████╔╝░██╔████╔██║███████║██║░░██╗░█████╗░░
██╔═══╝░░░╚██╔╝░░██║╚██╔╝██║██╔══██║██║░░╚██╗██╔══╝░░
██║░░░░░░░░██║░░░██║░╚═╝░██║██║░░██║╚██████╔╝███████╗
╚═╝░░░░░░░░╚═╝░░░╚═╝░░░░░╚═╝╚═╝░░╚═╝░╚═════╝░╚══════╝

Cipher : 1
Decipher : 2

Info :

    Pymage is a full open source software which allows you to encrypt your message in a random picture to send it to your friends.
    Therefore, they will be able to decrypt your message !
    
    Coded by A70m0s
    Gitlab : gitlab.com/A70m0s
    Pylint rated : 8.83/10

'''

alphabet = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7,
            'h': 8, 'i': 9, 'j': 10, 'k': 11, 'l': 12, 'm': 13, 'n': 14,
            'o': 15, 'p': 16, 'q': 17, 'r': 18, 's': 19, 't': 20, 'u': 21,
            'v': 22, 'w': 23, 'x': 24, 'y': 25, 'z': 26, ' ': 27, '\'': 28, ',': 29, '.': 30}


def menu():

    """Interactive menu"""

    print (BANNER)
    answer_menu = int(input("Number you want : "))
    if answer_menu not in (1, 2):
        print('Retry with a correct number')
        sys.exit()

    # Open user image

    img_path = input("Your file path : ")
    image = Image.open(img_path)

    # Message from user

    msg = input("Your message (if not, press ENTER) : ")
    len_msg = len(msg)

    # Save image size

    width, height = image.size
    if answer_menu == 1:
        cipher(msg, image, width, height, len_msg)
        print('Your image is ready to send !')
    if answer_menu == 2:
        print('\nDecrypted message :\n\n', decipher(image, width, height), '\n\nGood bye !\n')
    return 0


def check_size(width, height, len_msg):

    """Verify if image size is good"""

    if width <= len_msg and height <= len_msg:
        return False
    check_size(width, height, len_msg)
    return 0


def cipher(msg, image, width, height, len_msg):

    """Cipher function"""

    pi_modif = ''
    x_pixel = 0
    y_pixel = 0
    (le_r, nm_g, sg_b) = image.getpixel((width - 1, height - 1))
    image.putpixel((width - 1, height - 1), (len_msg, nm_g, sg_b))
    msg = msg.lower()
    for i in msg:
        (pi_r, xe_g, l_b) = image.getpixel((x_pixel, y_pixel))
        pi_modif = alphabet[i]
        image.putpixel((x_pixel, y_pixel), (pi_modif + 125, xe_g, l_b))
        if x_pixel + 50 > width:
            x_pixel = 0
            y_pixel += 50
        else:
            x_pixel += 50
        image.save("Toto.png")
    return 0


def decipher(image, width, height):

    """Decipher function"""

    d_msg = ''
    x_pixel = 0
    y_pixel = 0
    (pi_r, xe_g, l_b) = image.getpixel((x_pixel, y_pixel))
    (le_r, nm_g, sg_b) = image.getpixel((width - 1, height - 1))
    for z_test in range(le_r):
        for i in alphabet:
            if alphabet[i] == pi_r-125:
                d_msg = d_msg + i
        if x_pixel + 50 > width:
            x_pixel = 0
            y_pixel += 50
            (pi_r, xe_g, l_b) = image.getpixel((x_pixel, y_pixel))
        else:
            x_pixel += 50
            (pi_r, xe_g, l_b) = image.getpixel((x_pixel, y_pixel))
    return d_msg


if __name__ == '__main__':
    menu()
